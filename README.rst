Jello
=====

Jello is a minimalistic JavaScript library for lazy/asynchronous module
loading. Which basically means you just stick it in your project and loads any
other javascript files away whenever you need it.


Usage
=====

1. Stick Jello just before your ``</body>`` tag::

    <script src="path/to/jello.js"></script>

2. *Jello* your modules away::

    jello('A.js', 'B.js', function() {
        /* you can safely use A.js and B.js's functions here */
    });

You can take a look at the source code to see what other options are supported
by the ``jello`` function.


Getting the docs
================

Documentation uses `Sphinx`_, so once you have it, just go to the ``docs``
directory and type::

    $ make html

And this will generate the documentation in the ``_build/html`` directory.


.. _Sphinx: http://sphinx.pocoo.org/index.html


Acknowledgements
================

Holy thanks to ``fatbrain`` on freenode's ``##javascript`` channel, for
being nice and helping me see how much I tend to overengineer things that are
supposed to be minimalistic :3


Licence
=======

Licenced under WTFPL. So you just do whatever you fucking you want with the
library :3
