Howdy, there!
=============

Jello is a minimalist library for lazily loading scripts. It implements some
dependency handling and asynchronous loading, **all in less than 1kb**.

You can just clone the code in `Github`_ right away, or get the latest `stable
release`_

.. toctree::
   :maxdepth: 2

   using
   understanding
   api/jello
   dev/index


