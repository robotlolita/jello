Api documentation
=================

The API exposes only one front-end function, named ``jello``. Some additional
properties will be available if you're either using the source file directly —
without building — or if you've built it with debugging support.


Functions
---------

.. function:: jello(scripts_or_options...[, callback]) -> undefined
   
   Imports a javascript module, unless it has been previously loaded through
   this function.

   :param scripts_or_options:
       Positional arguments with the URLs of the scripts to import, and
       optional option objects.
   :param callback:
       Function to be called (with no arguments) when all the given modules are
       loaded.
   
You can pass any number of modules as positional arguments and they'll be
imported in the exactly order they've been declared. This means that if you
pass the function ``A.js`` and ``B.js``, the function will load ``A.js``,
and after it's fully loaded, it'll load ``B.js``.

If you pass any ``Object`` in the positional arguments, Jello will assume
that as additional loading options for the next group of modules. You may
pass as many option objects as you need. See :ref:`importing-options` for a
list of accepted parameters.
   
This particular argument parsing used by Jello allows for a quite powerful
grouping syntax that can help you spare some function calls.

For example, if you needed to load a handful of modules from different
locations you could go from this::

    jello({path: '/lib'}, 'A.js', 'B.js', function() {
        jello({path: '/include'}, 'C.js', 'D.js', function() {
            /* your code here */
        })
    })

To this::

    jello({path: '/lib'},     'A.js', 'B.js'
         ,{path: '/include'}, 'C.js', 'D.js'
         ,function() { /* your code here */ })



.. _importing-options:

Importing Options
-----------------

Jello accepts a few options to control how the modules should be imported:

.. describe:: path {String}

   :default: ``"./"``

   String with a URL for which all the imported scripts will be preceded.


.. describe:: async {Boolean}

   :default: ``false``

   Defines whether the script will be loaded asynchronously or not.

   If ``true``, Jello will try to load the script asynchronously and **won't
   wait for its loading** before importing other scripts.

   This is quite faster than loading synchronously, and it's useful when you
   want to load scripts that aren't dependencies for others.


.. describe:: force {Boolean}

   :default: ``false``

   Forces the script to be imported even if it has been before. Use this if
   you need to override a previously loaded script with a new version.

   .. warning::
      It's important to note that you shouldn't use this option if you have
      circular references — as in the example above —, otherwise you'll put
      Jello in an infinite loop.

      But again, you shouldn't even be having circular references in your
      scripts...


Debugging
---------

Jello exposes some objects to help debugging script loading. These are accessed
as properties of the ``jello`` function, and will only be available if you're
using the unprocessed source file or a build version with debugging enabled
(See :ref:`advanced-build-instructions` for information on how to do this).


.. attribute:: modules

   An object mapping each imported module (by the full path) to a boolean
   indicating whether the module has been loaded or not.


.. attribute:: dependencies() → Number

   A function that returns the number of dependencies that Jello still has to
   load.


.. attribute:: callbacks

   A list of all the callbacks that still have to be called.


