Understanding Jello
===================

This page delineates the general design goals for Jello and its inner
workings.


Why Jello?
----------

There are so many other JavaScript libraries that work for the same purposes:
loading scripts from inside your scripts. And these libraries, like RequireJS_,
LABjs_, WhateverJS, are quite good indeed.

So, why creating yet-another-javascript-library-tm if clearly
there’s-a-jquery-plugin-for-that? Well, the thing about Jello is that it tries
to solve the problem in the simplest way possible. So while LABjs and RequireJS
have all the fancy stuff and all, they’re also big and kinda bloated.

The current Jello script, minified, is less than 1kb, while LABjs and RequireJS,
compiled with the same closure compiler and options, are respectively 4338
bytes and 12001 bytes. And now, I’m not saying that a big size is inherently a
bad thing, but sometimes you just don’t need all those fancy stuff (hence my
previous “bloated” comment.)

So, that’s where Jello comes in.


.. _RequireJS: http://requirejs.org/
.. _LABjs:     http://labjs.com/


Dependency handling
-------------------

Jello's dependency handling system is really simple: every time you require a
new script, the callback for that script — the function that gets called when
the script finishes loading, — is put on the stack. After Jello loads all the
scripts, it simply goes calling the functions in the stack.

Imagine a scenario where you have script ``A.js`` loading either ``C.js`` and
``B.js``. While ``C.js`` and ``B.js`` both require ``D.js``. The following
graph summarises this:

.. figure:: images/jello-dependency.png

   Dependency graph for the example

Internally we have the following stack with the references of the functions
that should be called:

.. figure:: images/jello-stack.png

   Callback stack for the example

As you can see, every callback is included just once in the stack.

When all the modules are loaded, Jello simply gets the last item put on the
stack (which in this case is the D.js callback) and calls the function,
repeating this until there are no more items on the stack.
