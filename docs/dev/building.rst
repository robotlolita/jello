.. _advanced-build-instructions:

Making custom builds
====================

Make sure you have all the :ref:`pre-requisites` for building Jello.

The library uses Python's Waf building system to automate the build
proccess. The tool is bundled with the source.

To compile the source:

.. code-block:: bash

   $ cd path/to/jello
   $ ./waf configure
   $ ./waf build


Build options
-------------

Aside from the options defined by Waf itself, you can use the following to
configure the build:


--output            Output filename. Defaults to 'jello.min.js' 

--debug             Whether to keep Jello's debugging symbols in the dist
                    script or not. 

--optimization      Closure compiler optimization level.

                    Can be one of the following:

                    - WHITESPACE_ONLY
                    - SIMPLE_OPTIMIZATIONS
                    - ADVANCED_OPTIMIZATIONS

                    The latter is pretty aggressive, minifying even the names
                    of the exposed variables. Use with care, and make sure you
                    test whether it didn't break anything.

                    Defaults to 'SIMPLE_OPTIMIZATIONS'


For the full list of options you can use, just run ``./waf --help``.


Notes for Windows users
-----------------------

Since Windows has this weird thing about using the file extension to assign
programs to execute them, you'll either need to type ``python waf <command>``
or ``waf.py <command>`` — assuming you've assigned the Python interpreter to
execute ``.py`` files.

Also, make sure the Python interpreter is in your ``PATH``, otherwise you'll
need to type the full path to the interpreter.

I'm also unsure whether GPP would run on Windows or not, so you're on your own
there for finding it or compiling it.
