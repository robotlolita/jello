Contributing with Jello
=======================

You can contribute with Jello in quite a few ways:

**Use it and tell what you think!**
    Perhaps one of the most important ways of contributting with the
    library. And it's quite easy too, so just do it :3

    By telling how and why you use the library, it can improve to meet your
    expectations better. So, it's quite the win-win thingie.


**Report bugs**
    Bugs are really, really nasty things. I don't like them, and I suppose you
    don't either, so everytime you see one, submit a proper bug report so we
    can get those things out of the way.

    See :ref:`reporting-bugs` for the way you should do it.


**Submit patches/feature requests**
    Although Jello's supposed to be kept focused and with a minimum of
    overhead, if some features meet the design they should be included.

    Patches for bugfixes and stuff are always appreciated.



.. _reporting-bugs:

Reporting bugs
--------------

Github's `issue tracker`_ is used for bug reports. When submitting a but, make
sure you give enough information on how to reproduce it. Basically, try to
include these tiny bits of information:

**Platform**
    Which browser (with the version, of course) and OS has the problem.

**Steps to reproduce the error**
    A little list of steps you should take to reproduce the error. Optionally
    you can just submit a minimal test case (JSFiddle_ is good for that!). Test
    cases are always nice :3


.. _issue tracker: https://github.com/killdream/Jello/issues 
.. _JSFiddle:      http://jsfiddle.net/


Submiting patches
-----------------

If you want to submit patches for Jello, just fork the repository, do your
changes and then send me a pull request.

Make sure you get yourself familiar with the tools used for building and the
repository structure too.


.. _repository-structure:

How the repository is organized
-------------------------------

The current development workflow uses two main branches:

- ``default``  — where development is carried (this is ``master`` in Git)
- ``gh-pages`` — for github project pages

Releases are tagged in the repository, using the following version convention:
``{major}.{minor}.{bugfix}{flags}``.

Major indicates a major line release. Minor indicates new features added. And
bugifixes are, well, bugfix releases.

Flags mark the type of release:

- ``a`` — alpha release
- ``b`` — beta release
- ``rc`` — release candidate

Stable releases have no flags.

Each stable release gets a long-lived branch of its own, so all bugfixes go in
there, these bugfixes are then merged back into ``default``, so the main branch
is always up to date.
