.. _pre-requisites:

Pre-requisites
==============

Before diving into Jello's code, you'll need some tools. Those are, however,
quite easy to get and use.


Get the code
------------

For Jello's development I use a Git_ repository hosted on Github_. But I don't
use **Git** for development, instead I prefer using Mercurial_ with the hg-git_
extension.

Which one you should use it's up to you, as long as you can interact with a Git
repository.

To get started, just clone the code on Github

.. code-block:: bash

   # you can use Git
   $ git clone git://github.com/killdream/Jello.git jello
   
   # or Mercurial + hg-git
   $ hg clone git://github.com/killdream/Jello.git jello


Whatever the tool you're using, you should have a repository with Jello's
source code now. You should read up on :ref:`repository-structure` before
hacking in though.


.. _Git:       http://git-scm.com/
.. _Mercurial: http://mercurial.selenic.com/
.. _hg-git:    http://hg-git.github.com/


Build tools
-----------

Jello uses the Waf_ building system, which is Python_ based. Waf is shipped
with the source, so you don't need to download it. You do, however, need
Python.

There are quite some other tools you'll need to use for building Jello's source
code:

**GPP**
    Get it from here: http://en.nothingisreal.com/wiki/GPP

    The General Purpose Preprocessor is used in Jello to include optional
    sections in the code. It's not actually a requirement to make the
    distribution files, but if you don't use it, you'll end up with additional
    debugging symbols and instructions in your minified script. Which is not
    really nice.


**Closure Compiler**
    Closure is also shipped with the code, so you don't need to worry about
    it. It is dependent on JRE, though.


**Java Runtime Environment**
    Get it from here:
    http://www.oracle.com/technetwork/java/javase/downloads/index.html

    Closure is written in Java, and as such, you'll need the JRE installed in
    your computer to run that thing. Chances are you already have it installed,
    but if you don't (and the configure script complains) you know where to go.


.. _Waf:    http://code.google.com/p/waf/
.. _Python: http://www.python.org/
