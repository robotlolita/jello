Getting started
===============

This page will walk you through the necessary steps to get Jello working. This
is meant as a fairly quick introduction, if you want to hack on the code and
build it yourself, we've also got :ref:`advanced-build-instructions` covered!


What you'll need
----------------

First step first. You'll need to download the Jello script. To do so, simply
grab the lastest `stable release`_.

Once you get the ``.js`` file, just upload it to your server and include it in
your page.


Installing
----------

To *install* Jello in any page you like, you just need to stick it at the
right end of your ``<body>`` tag. This will also ensure that `your pages load
faster`_:

.. code-block:: html

	<!DOCTYPE html>
	<html>
	<head> <!-- ... --> </head>
	<body>
		<!-- ... -->
		<script src="path/to/jello.js"></script>
		<script src="path/to/your/other/jsfiles.js"></script>
	</body>
	</html>

.. _your pages load faster: http://developer.yahoo.com/performance/rules.html#js_bottom


Using
-----

After the script is ready, you can call the main API function from any other
script in your page. Just do it::

	jello('A.js', 'B.js', function() {
		/* You can use any global function defined
		 * in either B.js and A.js here.
		 */
	})
