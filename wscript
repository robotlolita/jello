#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Configuration script for Waf building system.
"""

import Utils, Options, os, tempfile

top = '.'    # top directory
out = 'dist' # build directory

APPNAME = 'Jello.js'
VERSION = '0.6a'

# --[ build options ]----------------------------------------------------------
def set_options(ctx):
    ctx.add_option('--output', action='store', default='jello.min.js',
                   help="Output filename. Defaults to `jello.min.js`.")
    ctx.add_option('--debug', action='store', default=False,
                   help="Keep Jello's debug interface in dist file.")
    ctx.add_option('--optimization', action='store',
                   default='SIMPLE_OPTIMIZATIONS',
                   help="Closure compiler optimization level. Can be either "
                       +"WHITESPACE_ONLY, SIMPLE_OPTIMIZATIONS or "
                       +"ADVANCED_OPTIMIATIONS (which will also change the "
                       +"interface variablles). "
                       +"Defaults to SIMPLE_OPTIMIZATIONS.")
    

# --[ configure ]--------------------------------------------------------------
def configure(ctx):
    print("→ configuring the project...")
    ctx.env.OUTPUT = Options.options.output
    ctx.env.DEBUG  = Options.options.debug
    ctx.env.OO     = Options.options.optimization
    
    # check for programs
    ctx.find_program('java', var='JAVA', mandatory=True)
    ctx.find_program('gpp', var='GPP')

    print("Execute './waf build' to create the distribution files.")

# --[ build ]------------------------------------------------------------------
def build(ctx):
    # closure compiling
    def jccompile(task):
        src = task.inputs[0].srcpath(task.env)
        out = task.outputs[0].bldpath(task.env)

        if ctx.env.GPP:
            f = tempfile.NamedTemporaryFile()
            src = gpptext(f.name, src)
            
        cmd = (("%(JAVA)s -jar %(COMP)s --js_output_file %(TGT)s "
              + "--compilation_level %(OO)s --js %(SRC)s")
              % {"JAVA": ctx.env.JAVA,
                 "COMP": os.path.join(ctx.path.abspath(),
                                      "vendor/closure/compiler.jar"),
                 "SRC":  src,
                 "TGT":  out,
                 "OO":   ctx.env.OO })
        return Utils.exec_command(cmd)

    # preprocessing
    def gpptext(out, src):
        cmd = ctx.env.GPP
        for envar in ctx.env.get_merged_dict().keys():
            if ctx.env[envar]:
                cmd += " -D" + envar + "=" + str(ctx.env[envar])
        cmd += (' -U "//~" "" "(" "," ")" "(" ")" "#" "" '
               +' -M "//~" "\\n" " " " " "\\n" "" "" '
               +'-o ' + out + ' < ' + src)
        Utils.exec_command(cmd)
        return out
        


    print("→ building project files...")
    ctx(rule = jccompile,
        always = True,
        source = "src/jello.js",
        target = ctx.env.OUTPUT)





