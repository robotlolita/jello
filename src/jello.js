// Copyright (c) 2010 - Quildreen <quildreen@gmail.com>
//
// This library is free software. It is provided "as is" and without any
// warranty.
//
// You can do whatever you want with it under the terms of the WTFPL, version
// 2. Just take a peek at the `LICENCE.txt' shipped with the file for less
// details.

/******************************************************************************
 * What is Jello?                                                             *
 * --------------                                                             *
 *                                                                            *
 * Jello is a minimalistic javascript library for lazily loading scripts in   *
 * the browser. It can also do asynchronous loading and dependency handling   *
 * with quite some syntatic sugar :3                                          *
 ******************************************************************************/
(function(g) {
	var modules      = {} // map of modules required
	  , pending      = [] // list of script tags to add to the DOM
	  , dependencies = 0  // number of dependencies to load
	  , callbacks    = [] // pending callbacks

	  , readystate   = "onreadystatechange"
	  , doc          = document
	  , base         = doc.getElementsByTagName("head")[0]
	  , classOf      = ({}).toString // same as Object.prototype.toString
	  , slice        = [].slice

	  /** @const */
	  , _true = 1, _false = 0


	/*************************************************************************
	 * :: function ok() -> undefined                                         *
	 *                                                                       *
	 * Called when a new module has been loaded.                             *
	 *                                                                       *
	 * Takes care of loading the next module in the pending list, if any, or *
	 * calling the pending callbacks otherwise.                              *
	 *************************************************************************/
	function ok(ev) {
		var el, script
		// Removes the old script tag, if any
		el = ev.target || ev.srcElement
		el.parentNode && el.parentNode.removeChild(el)

		// continues on to loading the next modules
		script = pending.shift()
		if (script) base.appendChild(script)
		if (--dependencies <= 0)
			while (callbacks.length)
				callbacks.pop()()    // pops function, then call it
	}

	/************************************************************************
	 * :: function sanitize_path(path) -> String                            *
	 *    - {String} path: path to sanitize.                                *
	 *                                                                      *
	 * Asserts the given path always have a trailing slash.                 *
	 ************************************************************************/
	function sanitize_path(path) {
		return path + ((/\/$/.test(path)) ? "" : "/")
	}


	/**************************************************************************
	 * :: function jello(modules_or_options...[, callback]) -> undefined      *
	 *    - {String|Object} modules_or_options: Positional arguments defining *
	 * the modules to load, and optional parameters for how to load them.     *
	 *                                                                        *
	 *    - {Function} callback: function to call when all given modules are  *
	 * loaded.                                                                *
	 *                                                                        *
	 *                                                                        *
	 * Imports a javascript module. If it wasn't loaded yet, we'll load it.   *
	 *                                                                        *
	 * You can pass a range of modules on the arguments function, they'll be  *
	 * imported in the order they were declared.                              *
	 *                                                                        *
	 * If you pass any Object in the positional arguments, Jello assumes that *
	 * as parameters for loading the next modules. You can pass as many       *
	 * option objects as you need.                                            *
	 *                                                                        *
	 * Available options are:                                                 *
	 *                                                                        *
	 * :``path`` *(String)*: Optional path to prepend all following modules   *
	 * with.                                                                  *
	 *                                                                        *
	 * :``async`` *(Boolean)*: Whether to stick with the declared import      *
	 * order or not for the following modules.                                *
	 *                                                                        *
	 *     Setting ``async`` to ``true`` means that your modules will be      *
	 * imported without waiting for the previous ones, which is faster.       *
	 *                                                                        *
	 * :``force`` (Boolean): Imports the module even if it has already been   *
	 * loaded.                                                                *
	 *                                                                        *
	 *     Use with care, if you force the importing of a module that uses    *
	 * circular references, you'll end up with an endless dependency loop.    *
	 **************************************************************************/
	function require() {
		var module, elm, i
		  , options  = {}
		  , first    = _true
		  , args     = slice.call(arguments)
		  , len      = args.length
		  , callback = args.pop()

		// If we didn't get a callback, put it back in the args to iterate
		// Otherwise put it in the stack of pending callbacks
		if (typeof callback != "function") args.push(callback)
		else {
			callbacks.push(callback)
			len-- }

		for (i = 0; i < len; ++i) {
			module = args[i]

			// If it's not a String, it's expected to be an object that defines
			// properties for the next modules.
			if (classOf.call(module) != "[object String]") {
				options = module
				continue }

			// If module has already been loaded, don't load it again unless
			// the caller explicitly asks us to do so
			if (modules[module] && !options.force)
				continue

			// Sanitizes the given path, if any
			if (options.path)
				module = sanitize_path(options.path) + module

			// Sets up the module to be loaded, and increments dependencies so
			// we know we've still got stuff to wait for
			dependencies++
			elm       = doc.createElement("script")
			elm.async = options.async

			if (readystate in elm)
				/* IE uses readystatechange, so we set onreadystatechange to
				 * leave M$ fags happy, rite? Then Opera goes and implements
				 * both `onreadystatechange` and `onload`.
				 *
				 * Can I kill myself in less painful ways now?
				 */
				elm[readystate] = function(e) {
					if (/loaded|complete/.test(this.readyState)) {
						ok(e || window.event)
						this[readystate] = null }}
				else
					elm.onload = ok

			elm.src         = module
			modules[module] = _true
			if (first || options.async) {
				first = _false
				base.appendChild(elm) }
			else
				pending.push(elm) }
	}

	//~ifdef DEBUG
	require.modules      = modules
	require.dependencies = function(){ return dependencies }
	require.callbacks    = callbacks
	//~endif

	g.jello = require
}(window));